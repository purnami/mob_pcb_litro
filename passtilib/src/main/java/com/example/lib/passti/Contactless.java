package com.example.lib.passti;

import android.content.Context;
import android.hardware.usb.UsbDeviceConnection;
import android.nfc.NfcAdapter;
import android.nfc.NfcManager;
import android.util.Log;

import com.example.lib.helper.Hex;
import com.hoho.android.usbserial.driver.UsbSerialPort;

import java.io.IOException;
import java.math.BigInteger;

import static com.example.lib.helper.Hex.decodeL;
import static com.example.lib.helper.Hex.doWrite;
import static com.example.lib.helper.Hex.encodeL;
import static com.example.lib.helper.Hex.hexStringToByteArray;

public class Contactless {
    Context ctx;
    UsbSerialPort port = null;
    UsbDeviceConnection connection=null;
    private boolean modeOtg = false;
    private NfcAdapter nfcAdapter;
    Usb usb;
    boolean readidContinue = false;

    private static String LUMINOS_AID = "A000000571504805";
    private static String APDU_SELECT_FILE = "00A4040008";
    private static String APDU_READ_BALANCE = "904C000004";

    public Contactless(Context ctx){
        this.ctx=ctx;
    }

    public int init(UsbSerialPort usbSerialPort, UsbDeviceConnection connection){
        nfcAdapter=((NfcManager) ctx.getSystemService(Context.NFC_SERVICE)).getDefaultAdapter();
        if (nfcAdapter == null) {
            modeOtg = true;
        }
        this.port=usbSerialPort;
        this.connection=connection;
        setupPort(this.port, this.connection);
        return 0;
    }

    private void readid() {
        readidContinue=true;
        new Thread(new Runnable() {
            @Override
            public void run() {
                readIdLoop();
            }
        }).start();
    }

    private final synchronized void readIdLoop() {
        Log.d("readmasuk", "2");
        String ss = decodeL(doWrite(encodeL("40"), port));
        Log.d("sss", "" + ss);
        if (ss != null && ss.contains("9000") && ss.length() >=4) {
            Log.d("readmasuk", "3");
            readidContinue=false;
            checkBal();
        }

        if (readidContinue) {
            Log.d("readmasuk", "4");
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            readIdLoop();
        }
    }

    private void checkBal() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.d("modeotgg", ""+modeOtg);
                String selectfile = transceiveNFC(APDU_SELECT_FILE + LUMINOS_AID, 4, 0);
                Log.d("selectfile", ""+selectfile);
                String balance = transceiveNFC(APDU_READ_BALANCE, 4, 0);
                Log.d("balance", ""+balance);
                BigInteger in=new BigInteger(hexStringToByteArray(balance));
                Log.d("balance2", ""+in.toString());
            }
        }).start();
    }

    private String transceive(byte[] input){
        Log.d("inputtransceive", ""+Hex.bytesToHexString(input));
        Log.d("masukkedecode","1");
        return decodeL(doWrite(input, port));
    }
    private String transceiveNFC(String input, int minLength, int currentLoop){
        Log.d("inputt", ""+input);
        String ret = null;
        Log.d("masuknfc","1");
        if(modeOtg){
            ret=transceive(encodeL("30" + input.trim())); //transceive
            System.out.println("rett "+ret);
            Log.d("rett", ""+ret);
            if(ret!=null&&ret.endsWith("9000")){
                ret=ret.substring(0, ret.length()-4);
            }
        }
        if(ret!=null&&ret.length()>=minLength){
            Log.d("tesmasuk", "5");
            Log.d("final ret1", ""+ret);
            return ret;
        }
        return ret;
    }

    private void setupPort(UsbSerialPort usbSerialPort, UsbDeviceConnection connection) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                port=usbSerialPort;
                try {
                    port.open(connection);
                    port.setParameters(38400,
                            UsbSerialPort.DATABITS_8,
                            UsbSerialPort.STOPBITS_1,
                            UsbSerialPort.PARITY_NONE);
                    byte[] a = encodeL("10");
                    Log.d("encodel1",""+Hex.bytesToHexString(a));
                    byte[] b= doWrite(a, port);
                    Log.d("dowrite1",""+Hex.bytesToHexString(b));
                    String c= decodeL(b);
                    Log.d("decodel1",""+c+"  "+ decodeL(b));

                    if(!c.contains("9000")){
                        Log.d("masuk error", "1");
                        throw new IOException("INIT NFC FAILED");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public int cekBalance() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                setupPort(port, connection);
                Log.d("masukcekbal", "1");
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        if(modeOtg&&!readidContinue){
                            readid();
                        }
                    }
                }).start();
            }
        }).start();
        return 0;
    }

    public byte[] sendCTL(String apdu){
//        setupPort(port, connection);
        String ret = transceive(encodeL("30" + apdu.trim()));
        Log.d("rapdusendctl", ""+ret);
        return hexStringToByteArray(ret);
    }
}
