package com.example.lib.passti;

import android.content.Context;
import android.content.Intent;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.util.Log;

import com.hoho.android.usbserial.driver.ProbeTable;

import static com.example.lib.helper.ErrorCode.NOT_OK;
import static com.example.lib.helper.ErrorCode.OK;

public class Reader {
    UsbManager usbManager=null;
    UsbDevice usbDevice=null;
    Context ctx;
    ProbeTable customTable;
    Usb usb;
    Contactless contactless;
    Sam sam;
    Intent intent;

    public Reader(Context ctx){
        this.ctx=ctx;
        usb=new Usb(ctx);
        contactless=new Contactless(ctx);
        sam=new Sam(ctx);
    }

    public int init(){
        Log.d("masuk","1");
//        nfc.init();
        int result=NOT_OK;
        result = usb.init();
        Log.d("resultinitusb", ""+result);
        if(result!=OK){
            return result;
        }
        Log.d("masukctl", "1");
        result = contactless.init(usb.getUsbSerialPort(), usb.getConnection());
        Log.d("resultinitctl", ""+result);
        if(result!=OK){
            return result;
        }
        result = sam.init(usb.getUsbSerialPort(), usb.getConnection());
        Log.d("resultinitsam", ""+result);
        if(result!=OK){
            return result;
        }
        return OK;
    }

    public int cekBalance() {
        int result=NOT_OK;
        result=contactless.cekBalance();
        return result;
    }

    public int deduct() {
        int result=NOT_OK;
        result=sam.deduct();
        return result;
    }

    public byte[] sendCTL(String apdu) {
        return contactless.sendCTL(apdu);
    }

    public byte[] sendSAM(String apdu) {
        return sam.sendSAM(apdu);
    }
}
