package com.example.lib.passti;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;
import android.nfc.NfcAdapter;
import android.nfc.NfcManager;
import android.util.Log;

import java.math.BigInteger;

import com.example.lib.helper.Hex;
import com.hoho.android.usbserial.driver.CdcAcmSerialDriver;
import com.hoho.android.usbserial.driver.ProbeTable;
import com.hoho.android.usbserial.driver.UsbSerialDriver;
import com.hoho.android.usbserial.driver.UsbSerialPort;
import com.hoho.android.usbserial.driver.UsbSerialProber;

import java.io.IOException;
import java.util.List;

public class Usb {
    UsbManager usbManager=null;
    UsbDevice usbDevice=null;
    Context ctx;
    Intent intent;
    ProbeTable customTable;
    PendingIntent permissionIntent;

    private NfcAdapter nfcAdapter;

    private final String ACTION_USB_PERMISSION = "com.android.example.USB_PERMISSION";
    private static String LUMINOS_AID = "A000000571504805";
    private static String APDU_SELECT_FILE = "00A4040008";
    private static String APDU_READ_BALANCE = "904C000004";

    private boolean modeOtg = false;
    UsbSerialPort port = null;
    boolean readidContinue = false;

    private UsbDeviceConnection connection;
    private UsbSerialPort usbSerialPort;

    public UsbDeviceConnection getConnection() {
        return connection;
    }

    public void setConnection(UsbDeviceConnection connection) {
        this.connection = connection;
    }

    public UsbSerialPort getUsbSerialPort() {
        return usbSerialPort;
    }

    public void setUsbSerialPort(UsbSerialPort usbSerialPort) {
        this.usbSerialPort = usbSerialPort;
    }

    public Usb(Context ctx){
        this.ctx=ctx;
//        this.intent=intent;
    }

    public void initNfc(){
        nfcAdapter=((NfcManager) ctx.getSystemService(Context.NFC_SERVICE)).getDefaultAdapter();
        if (nfcAdapter == null) {
            modeOtg = true;
            return;
        }
    }

    public int init(){
        Log.d("masuk","2");
//        nfcAdapter=((NfcManager) ctx.getSystemService(Context.NFC_SERVICE)).getDefaultAdapter();
//        if (nfcAdapter == null) {
//            modeOtg = true;
//        }

        usbManager=(UsbManager) ctx.getSystemService(Context.USB_SERVICE);

        permissionIntent = PendingIntent.getBroadcast(ctx, 0, new Intent(ACTION_USB_PERMISSION), 0);
        IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
        ctx.registerReceiver(usbReceiver, filter);

        customTable = new ProbeTable();
        customTable.addProduct(1241, 46388, CdcAcmSerialDriver.class);

        UsbSerialProber prober = new UsbSerialProber(customTable);
        List<UsbSerialDriver> drivers = prober.findAllDrivers(usbManager);
        if (drivers.isEmpty()) {
            return -1;
        }
        UsbSerialDriver driver = drivers.get(0);
        if (usbDevice == null) {
            usbDevice = driver.getDevice();
            Log.d("vendorId",""+driver.getDevice().getVendorId());
            Log.d("productId",""+driver.getDevice().getProductId());
        }
        if (!usbManager.hasPermission(usbDevice)) {
            usbManager.requestPermission(usbDevice, permissionIntent);
        }

        //balance
        connection = usbManager.openDevice(usbDevice);
        setConnection(connection);
        if (connection == null) {
//            nfcReaderSync = null;
            port = null;
        }

        usbSerialPort = driver.getPorts().get(0);
        setUsbSerialPort(usbSerialPort);
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                setupPort(usbSerialPort, connection);
//                Log.d("masukcekbal", "1");
//                new Thread(new Runnable() {
//                    @Override
//                    public void run() {
//                        if(modeOtg&&!readidContinue){
//                            readid();
//                        }
//                    }
//                }).start();
//            }
//        }).start();
        return 0;
    }



    private void readid() {
        readidContinue=true;
        new Thread(new Runnable() {
            @Override
            public void run() {
                readIdLoop();
            }
        }).start();
    }

    private final synchronized void readIdLoop() {
        Log.d("readmasuk", "2");
        String ss = Hex.decodeL(Hex.doWrite(Hex.encodeL("40"), port));
        Log.d("sss", "" + ss);
        if (ss != null && ss.contains("9000") && ss.length() >=4) {
            Log.d("readmasuk", "3");
            readidContinue=false;
            checkBal();
        }

        if (readidContinue) {
            Log.d("readmasuk", "4");
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            readIdLoop();
        }
    }

    private void checkBal() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.d("modeotgg", ""+modeOtg);
                String selectfile = transceiveNFC(APDU_SELECT_FILE + LUMINOS_AID, 4, 0);
                Log.d("selectfile", ""+selectfile);
                String balance = transceiveNFC(APDU_READ_BALANCE, 4, 0);
                Log.d("balance", ""+balance);
                BigInteger in=new BigInteger(Hex.hexStringToByteArray(balance));
                Log.d("balance2", ""+in.toString());
            }
        }).start();
    }
    private String transceive(byte[] input){
        Log.d("inputtransceive", ""+Hex.bytesToHexString(input));
        Log.d("masukkedecode","1");
        return Hex.decodeL(Hex.doWrite(input, port));
    }
    private String transceiveNFC(String input, int minLength, int currentLoop){
        Log.d("inputt", ""+input);
        String ret = null;
        Log.d("masuknfc","1");
        if(modeOtg){
            ret=transceive(Hex.encodeL("30" + input.trim()));
            System.out.println("rett "+ret);
            Log.d("rett", ""+ret);
            if(ret!=null&&ret.endsWith("9000")){
                ret=ret.substring(0, ret.length()-4);
            }
        }
        if(ret!=null&&ret.length()>=minLength){
            Log.d("tesmasuk", "5");
            Log.d("final ret1", ""+ret);
            return ret;
        }
        return ret;
    }

    private void setupPort(UsbSerialPort usbSerialPort, UsbDeviceConnection connection) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                port=usbSerialPort;
                try {
                    port.open(connection);
                    port.setParameters(38400,
                            UsbSerialPort.DATABITS_8,
                            UsbSerialPort.STOPBITS_1,
                            UsbSerialPort.PARITY_NONE);
                    byte[] a = Hex.encodeL("10");
                    Log.d("encodel1",""+Hex.bytesToHexString(a));
                    byte[] b= Hex.doWrite(a, port);
                    Log.d("dowrite1",""+Hex.bytesToHexString(b));
                    String c= Hex.decodeL(b);
                    Log.d("decodel1",""+c+"  "+Hex.decodeL(b));

                    if(!c.contains("9000")){
                        Log.d("masuk error", "1");
                        throw new IOException("INIT NFC FAILED");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private final BroadcastReceiver usbReceiver=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(ACTION_USB_PERMISSION == intent.getAction()){
                synchronized (ctx){
                    UsbDevice device = (UsbDevice) intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                    if(intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)){
                        if(device!=null){
                            usbDevice=device;
                            Log.d("Permission allowed", ""+device);
                            if(modeOtg) init();
                        }
                    }
                    else {
                        Log.d("Permission denied", ""+device);
                    }
                }
            }
        }
    };
}
