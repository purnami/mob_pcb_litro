package com.example.lib.passti;

import android.content.Context;
import android.hardware.usb.UsbDeviceConnection;
import android.nfc.NfcAdapter;
import android.nfc.NfcManager;
import android.util.Log;

import com.example.lib.helper.Hex;
import com.hoho.android.usbserial.driver.UsbSerialPort;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Locale;

import static com.example.lib.helper.Hex.bytesToHexString;
import static com.example.lib.helper.Hex.decodeL;
import static com.example.lib.helper.Hex.doWrite;
import static com.example.lib.helper.Hex.encodeL;
import static com.example.lib.helper.Hex.hexStringToByteArray;

public class Sam {
    Context ctx;
    boolean readidContinue = false;
    UsbSerialPort port = null;
    UsbDeviceConnection connection=null;
    boolean continuePurchase = false;
    private boolean modeOtg = false;
    private NfcAdapter nfcAdapter;

    private static String LUMINOS_AID = "A000000571504805";
    private static String APDU_SELECT_FILE = "00A4040008";
    private static String APDU_READ_BALANCE = "904C000004";
    private static String LUMINOS_SAM_AID = "57100DEF000001";

    public Sam(Context ctx) {
        this.ctx = ctx;
    }

    public int init(UsbSerialPort usbSerialPort, UsbDeviceConnection connection) {
        nfcAdapter=((NfcManager) ctx.getSystemService(Context.NFC_SERVICE)).getDefaultAdapter();
        if (nfcAdapter == null) {
            modeOtg = true;
        }
        readidContinue=false;
        this.port=usbSerialPort;
        this.connection=connection;
        return 0;
    }

    private void readid() {
        readidContinue=true;
        new Thread(new Runnable() {
            @Override
            public void run() {
                readIdLoop();
            }
        }).start();
    }

    private final synchronized void readIdLoop() {
        Log.d("readmasuk", "2");
        String ss = decodeL(doWrite(encodeL("40"), port));
        Log.d("sss", "" + ss);
        if (ss != null && ss.contains("9000") && ss.length() >=4) {
            Log.d("readmasuk", "3");
            readidContinue=false;
            purchase(1);
        }

        if (readidContinue) {
            Log.d("readmasuk", "4");
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            readIdLoop();
        }
    }

    private void purchase(int i) {
        continuePurchase = true;
        readidContinue = false;
        new Thread(new Runnable() {
            @Override
            public void run() {
                String selectFile = transceiveNFC(APDU_SELECT_FILE + LUMINOS_AID, 4, 0);
                String cardId=selectFile.substring(10, 24);
                Log.d("PURCHASE SELECT FILE", (APDU_SELECT_FILE + LUMINOS_AID) + " -> " + selectFile+ "  cardid "+cardId);
                String balance = transceiveNFC(APDU_READ_BALANCE, 4, 0);
                Log.d("PURCHASE READ BALANCE", APDU_READ_BALANCE + " -> " + balance);

                long timelong = System.currentTimeMillis();
                String time = currentTime(timelong);
                String send = "9040030118" + amtHex(i) + time + "11111111111111111111111111".toUpperCase(Locale.getDefault());
                String initPurchase = transceiveNFC(send, 4, 0);
                Log.d("PURCHASE CTL INIT", send + " -> " + initPurchase);

                String selectSam = transceiveSAM("00A4040007" + LUMINOS_SAM_AID);
                Log.d("PURCHASE SAM SELECT", "00A4040007" + LUMINOS_SAM_AID + " -> " + selectSam);
                String sipcmd = ("800200001B" + initPurchase + amtHex(i) + "00").toUpperCase(Locale.getDefault());
                String saminitPurchase = transceiveSAM(sipcmd);
                while (saminitPurchase == null || saminitPurchase.length() <= 4) {
                    Log.d("PURCHASE SAM INIT1", sipcmd + " -> " + saminitPurchase);
                    saminitPurchase = transceiveSAM(sipcmd);
                    if(!continuePurchase){
                        Log.d("masukpurchase", "11");
                    }
                }
                Log.d("PURCHASE SAM INIT2", sipcmd + " -> " + saminitPurchase);
                String ctlpurchase = ("9046000012" + saminitPurchase.substring(0, saminitPurchase.length() - 4) + "04").toUpperCase(Locale.getDefault());
                String ctlpurchaseResp = transceiveNFC(ctlpurchase, 4, 0);
                Log.d("PURCHASE CTL DO PURCHAS", ctlpurchase + " -> " + ctlpurchaseResp);

                String creditSam = ("8004000004" + ctlpurchaseResp + "31").toUpperCase(Locale.getDefault());
                String creditSamResp = transceiveSAM(creditSam);
                Log.d("PURCHASE SAM CREDIT2", creditSam + " -> " + creditSamResp);
//                byte[] newcredit=hexStringToByteArray(creditSamResp);
//                byte[] newsaldo= Arrays.copyOfRange(newcredit)

            }
        }).start();
    }

    public final String amtHex(int amt) {
        String hex = Integer.toHexString(amt);
        Log.d("amttt", ""+amt);
        Log.d("amthex", ""+hex);
        Log.d("amtpad", ""+("00000000".substring(hex.length()) + hex));
        return "00000000".substring(hex.length()) + hex;
    }

    private String currentTime(long time) {
        return new SimpleDateFormat("yyyyMMddHHmmss").format(time);
    }

    private String transceive(byte[] input){
        Log.d("inputtransceive", ""+bytesToHexString(input));
        Log.d("masukkedecode","1");
        return decodeL(doWrite(input, port));
    }
    private String transceiveNFC(String input, int minLength, int currentLoop){
        Log.d("inputt", ""+input);
        String ret = null;
        Log.d("masuknfc","1");
        if(modeOtg){
            ret=transceive(encodeL("30" + input.trim()));
            System.out.println("rett "+ret);
            Log.d("rett", ""+ret);
            if(ret!=null&&ret.endsWith("9000")){
                ret=ret.substring(0, ret.length()-4);
            }
        }
        if(ret!=null&&ret.length()>=minLength){
            Log.d("tesmasuk", "5");
            Log.d("final ret1", ""+ret);
            return ret;
        }
        return ret;
    }

    private String transceiveSAM(String input){
        return transceive(encodeL(("31" + input.trim())));
    }

    public int deduct() {
        readid();
        return 0;
    }

    public byte[] sendSAM(String apdu) {
        String ret = transceive(encodeL(("31" + apdu.trim())));
        Log.d("rapdusendsam", ""+ret);
        return hexStringToByteArray(ret);
    }
}
