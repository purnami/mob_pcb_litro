package com.example.lib.passti;

import android.content.Context;
import android.util.Log;

import com.example.lib.card.Luminos;

import static com.example.lib.helper.ErrorCode.ERR_CARDTYPEINACTIVE;
import static com.example.lib.helper.ErrorCode.OK;
import static com.example.lib.passti.params.ctype_unknown;

public class STIUtility {
    Context ctx;
    Reader reader;
    Luminos luminos;

    int card = 0;
    int saldoAwal, saldoAkhir;

    public STIUtility(Context ctx){
        this.ctx=ctx;
        reader=new Reader(ctx);
    }

    public int initReader() {
        return reader.init();
    }

    public int cekBalance() {
        int i = ERR_CARDTYPEINACTIVE;
        Log.d("masukcekbalance","1");
        card = checkMyCard();
        Log.d("cardd", ""+card);
        switch (card){
            case params.ctype_luminos:
                i=luminos.cekBalance();
                break;
        }
        return i;
//        return reader.cekBalance();
    }

    private int checkMyCard() {
        Log.d("masukcekbalance","2");
        int i = ctype_unknown;
        i=luminos.selectApp();
        if(i==OK){
            return params.ctype_luminos;
        }
        return i;
    }

    public int deduct(int amount) {
        Log.d("nominal", ""+amount);
        int i = ERR_CARDTYPEINACTIVE;
        card = checkMyCard();
        Log.d("cardd", ""+card);
        switch (card){
            case params.ctype_luminos:
                i=luminos.deduct(amount);
                break;
        }
        return i;
//        return reader.deduct();
    }

    public int initPower(int card) {
        switch (card){
            case 1 :
                luminos=new Luminos(ctx, reader);
                break;

        }
        return 0;
    }

    public int getBalance() {
        switch (card){
            case 1:
                saldoAwal=luminos.getBalance();
                break;
        }
        return saldoAwal;
    }

    public int getDeduct() {
        switch (card){
            case 1:
                saldoAkhir=luminos.getDeduct();
                break;
        }
        return saldoAkhir;
    }
}
