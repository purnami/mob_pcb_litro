package com.example.lib.passti;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.lib.R;

public class BlankActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blank);
    }
}