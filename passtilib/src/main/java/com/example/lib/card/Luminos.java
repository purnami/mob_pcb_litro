package com.example.lib.card;

import android.content.Context;
import android.util.Log;

import com.example.lib.passti.Reader;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Locale;

import static com.example.lib.helper.ErrorCode.ERR_NO_RESP;
import static com.example.lib.helper.ErrorCode.OK;
import static com.example.lib.helper.Hex.bytesToHexString;
import static com.example.lib.helper.Hex.hexStringToByteArray;

public class Luminos {
    Context ctx;
    Reader reader;
    private byte[] rapdu=new byte[256];
    int saldoAwal=0;
    int saldoAkhir=0;

    private static String LUMINOS_AID = "A000000571504805";
    private static String APDU_SELECT_FILE = "00A4040008";
    private static String APDU_READ_BALANCE = "904C000004";
    private static String LUMINOS_SAM_AID = "57100DEF000001";

    public Luminos(Context ctx, Reader reader) {
        this.ctx = ctx;
        this.reader =reader;
    }

    public int getBalance(){
        return this.saldoAwal;
    }

    public int selectApp() {
        Log.d("masukselectapp","1");
        rapdu=reader.sendCTL(APDU_SELECT_FILE+LUMINOS_AID);
        Log.d("rapduselectapp",""+bytesToHexString(rapdu));
        if(rapdu==null){
            return ERR_NO_RESP;
        }
        return 0;
    }

    public int cekBalance() {
        saldoAwal=0;
        String ret;
        rapdu=reader.sendCTL(APDU_READ_BALANCE);
//        byte[] newsaldo= Arrays.copyOfRange(rapdu, rapdu[3], rapdu[7]);
        Log.d("newrapdu", ""+bytesToHexString(rapdu)+"   ");
        if(rapdu==null){
            return ERR_NO_RESP;
        }else {
            saldoAwal=setSaldoAwal(bytesToHexString(rapdu));
//            ret = bytesToHexString(rapdu);
//            if(ret.endsWith("9000")){
//                ret=ret.substring(0, ret.length()-4);
//                BigInteger in=new BigInteger(hexStringToByteArray(ret));
//                Log.d("balance2", ""+in.toString());
//                saldoAwal= in.intValue();
//                Log.d("Saldo awal", ""+saldoAwal);
//            }
        }
        return 0;
    }

    private int setSaldoAwal(String ret){
        int saldo = 0;
        if(ret.endsWith("9000")){
            ret=ret.substring(0, ret.length()-4);
            Log.d("rettt", ""+ret);
            BigInteger in=new BigInteger(hexStringToByteArray(ret));
            Log.d("balance2", ""+in.toString());
            saldo= in.intValue();
            Log.d("Saldo", ""+saldo);
        }
        return saldo;
    }

    public int deduct(int amount) {
        saldoAwal = 0;
        saldoAkhir = 0;
        int i = payment(amount);
        return i;
    }

    private int payment(int amount) {
        byte[] result;
        result = reader.sendCTL(APDU_READ_BALANCE);
        Log.d("APDU_READ_BALANCE", ""+bytesToHexString(result));
        saldoAwal=setSaldoAwal(bytesToHexString(result));
        Log.d("saldoawalpayment", ""+saldoAwal);
        if(result== null)
            return ERR_NO_RESP;
        result=initPurchase(amount);
        Log.d("resultinitpurchase", ""+bytesToHexString(result));
        byte[] initpurchase = Arrays.copyOfRange(result, 0,result.length-2);
        Log.d("initpurchase", ""+bytesToHexString(initpurchase));
        if(result== null)
            return ERR_NO_RESP;
        result=samSelectAid();
        if(result== null)
            return ERR_NO_RESP;
        result=samInitPurchase(initpurchase, amount);
        byte[] saminit = result;
        if(result== null)
            return ERR_NO_RESP;
        result=ctlPurchase(bytesToHexString(saminit));
        Log.d("resultctlpurse", ""+bytesToHexString(result));
        byte[] ctlpurchase = Arrays.copyOfRange(result, 0,result.length-2);
        if(result== null)
            return ERR_NO_RESP;
        result=samCredit(bytesToHexString(ctlpurchase));
        saldoAkhir=setSaldoAkhir(result);
        if(result== null)
            return ERR_NO_RESP;
        return OK;

    }

    private int setSaldoAkhir(byte[] ret){
        int saldo=0;
//        Arrays.copyOfRange(result[0], 8,16);
        ret=Arrays.copyOfRange(ret, 15, 19);
        Log.d("saldoakhir", ""+bytesToHexString(ret));
        BigInteger in=new BigInteger(ret);
        Log.d("balance2", ""+in.toString());
        saldo= in.intValue();
        Log.d("Saldo", ""+saldo);
        return saldo;
    }

    private byte[] samCredit(String ctlpurchase) {
        String creditSam = ("8004000004" + ctlpurchase + "31").toUpperCase(Locale.getDefault());
        rapdu=reader.sendSAM(creditSam);
        Log.d("PURCHASE SAM CREDIT2", creditSam + " -> " + bytesToHexString(rapdu));
        return rapdu;
    }

    private byte[] ctlPurchase(String saminit) {
        String ctlpurchase = ("9046000012" + saminit.substring(0, saminit.length() - 4) + "04").toUpperCase(Locale.getDefault());
        rapdu=reader.sendCTL(ctlpurchase);
        Log.d("PURCHASE CTL DO PURCHAS", ctlpurchase + " -> " + bytesToHexString(rapdu));
        return rapdu;
    }

    public final String amtHex(int amt) {
        String hex = Integer.toHexString(amt);
        Log.d("amttt", ""+amt);
        Log.d("amthex", ""+hex);
        Log.d("amtpad", ""+("00000000".substring(hex.length()) + hex));
        return "00000000".substring(hex.length()) + hex;
    }

    private byte[] samInitPurchase(byte[] initpurchase, int amount) {
//        String hex = Integer.toHexString(amount);
//        String amtHex = "0000".substring(hex.length()) + hex;
//        String sipcmd = ("800200001B" + bytesToHexString(initpurchase) + amtHex + "00").toUpperCase(Locale.getDefault());
        String sipcmd = ("800200001B" + bytesToHexString(initpurchase) + amtHex(amount) + "00").toUpperCase(Locale.getDefault());
        rapdu=reader.sendSAM(sipcmd);
        Log.d("PURCHASE SAM INIT", sipcmd + " -> " + bytesToHexString(rapdu));
        return rapdu;
    }

    private byte[] samSelectAid() {
        rapdu=reader.sendSAM("00A4040007" + LUMINOS_SAM_AID);
        Log.d("PURCHASE SAM SELECT", "00A4040007" + LUMINOS_SAM_AID + " -> " + bytesToHexString(rapdu));
        return rapdu;
    }

    private byte[] initPurchase(int amount) {
        long timelong = System.currentTimeMillis();
        String time = new SimpleDateFormat("yyyyMMddHHmmss").format(timelong);
        String hex = Integer.toHexString(amount);
        String amtHex = "00000000".substring(hex.length()) + hex;
        String send = "9040030118" + amtHex + time + "11111111111111111111111111".toUpperCase(Locale.getDefault());
        rapdu=reader.sendCTL(send);
        Log.d("PURCHASE CTL INIT", send + " -> " + bytesToHexString(rapdu));
        return rapdu;
    }

    public int getDeduct() {
        return this.saldoAkhir;
    }
}
