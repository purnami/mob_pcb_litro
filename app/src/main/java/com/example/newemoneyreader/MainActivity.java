package com.example.newemoneyreader;

import androidx.appcompat.app.AppCompatActivity;

import android.app.PendingIntent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.lib.passti.Reader;
import com.example.lib.passti.STIUtility;

import java.text.NumberFormat;
import java.util.Locale;

import static com.example.lib.helper.ErrorCode.ERR_UNKNOWN;
import static com.example.lib.helper.ErrorCode.OK;

public class MainActivity extends AppCompatActivity {
    Reader reader;
    PendingIntent pendingIntent;
    TextView tv;
    Button initReader, initPower, cekBalance, deduct;
    Spinner spinnerCard;
    EditText nominal;

    STIUtility sti;

    int card;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv=findViewById(R.id.tv);
        initReader=findViewById(R.id.initReader);
        initPower=findViewById(R.id.initPower);
        cekBalance=findViewById(R.id.cekbalance);
        deduct=findViewById(R.id.deduct);
        nominal=findViewById(R.id.nominal);
        spinnerCard=findViewById(R.id.spinnerCard);
        spinnerCard.setOnItemSelectedListener(cardSelected);

//        reader=new Reader(getApplicationContext());
        initReader.setOnClickListener(clickInitReader);
        initPower.setOnClickListener(clickinitPower);
        cekBalance.setOnClickListener(clickCekBalance);
        deduct.setOnClickListener(clickDeduct);
    }

    AdapterView.OnItemSelectedListener cardSelected=new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            switch(position)
            {
                case 0 :
                    card = 1;
                    break;
                case 1:
                    card = 2;
                    break;
                case 2:
                    card=3;
                    break;
                case 3:
                    card=4;
                    break;

            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    View.OnClickListener clickInitReader=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int result=ERR_UNKNOWN;
            if(sti==null){
                sti=new STIUtility(getApplicationContext());
                result=sti.initReader();
            }
        }
    };

    View.OnClickListener clickinitPower=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Log.d("cardd", ""+card);
            int result=ERR_UNKNOWN;
            result=sti.initPower(card);
        }
    };

    View.OnClickListener clickCekBalance=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Log.d("clickcekbalance", "1");
            int result=ERR_UNKNOWN;
            result=sti.cekBalance();
            Log.d("resultcekbal1", ""+result);
            if(result==OK){
                Locale locale = new Locale("id", "ID");
                NumberFormat format=NumberFormat.getCurrencyInstance(locale);
                showText("Saldo : "+format.format(sti.getBalance()));
            }
        }
    };

    View.OnClickListener clickDeduct=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Log.d("clickcekdeduct", "1");
            int result=ERR_UNKNOWN;
            String i = String.valueOf(nominal.getText());
            result=sti.deduct(Integer.parseInt(i));
            Log.d("resultdeduct1", ""+result);
            if(result==OK){
                Locale locale = new Locale("id", "ID");
                NumberFormat format=NumberFormat.getCurrencyInstance(locale);
                showText("Saldo Awal : "+format.format(sti.getBalance())+"\nSaldo Akhir : "+format.format(sti.getDeduct()));
            }
        }
    };

    private void showText(final String text)
    {
        new Thread() {
            public void run() {
                try {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            tv.setText(text);
                        }
                    });
                    Thread.sleep(300);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }.start();

    }

//    @Override
//    protected void onNewIntent(Intent intent) {
//        super.onNewIntent(intent);
//        setIntent(intent);
//
//
//    }

}